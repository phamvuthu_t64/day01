-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 12, 2022 lúc 05:51 PM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `qlsv`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dmkhoa`
--

CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `dmkhoa`
--

INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES
('QT111', 'Công nghệ thông tin'),
('QT112', 'Khoa học dữ liệu'),
('QT113', 'Quản lý đất đai'),
('QT114', 'Công nghệ sinh học'),
('QT115', 'Vật lý học');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sinhvien`
--

CREATE TABLE `sinhvien` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) DEFAULT NULL,
  `TenSV` varchar(15) DEFAULT NULL,
  `GioiTinh` char(1) DEFAULT NULL,
  `NgaySinh` datetime DEFAULT NULL,
  `NoiSinh` varchar(50) DEFAULT NULL,
  `DiaChi` varchar(50) DEFAULT NULL,
  `MaKH` varchar(6) NOT NULL,
  `HocBong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `sinhvien`
--

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('190001', ' Pham Van', 'Anh', NULL, '2001-05-16 13:15:15', 'Ha Noi', 'Quoc Oai', 'QH111', 0),
('190001', ' Nguyen Nhu', 'Hoa', NULL, '2002-02-17 10:15:10', 'Ninh Binh', 'Trang An', 'QH112', 0),
('190002', ' Do Thanh', 'Nhu', NULL, '2001-06-01 13:15:15', 'Ha Noi', 'Hang Duong', 'QH111', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `dmkhoa`
--
ALTER TABLE `dmkhoa`
  ADD PRIMARY KEY (`MaKH`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
